﻿using System.Data.Entity;

namespace Astarlearning.Entities
{
    public partial class AstarContext : DbContext
    {
        public virtual DbSet<CandidateEntity> Candidates { get; set; }

        public virtual DbSet<LocationEntity> Locations { get; set; }

        public virtual DbSet<SkillEntity> Skills { get; set; }

        public virtual DbSet<CandidateSkillLinkEntity> CandidateSkillLinks { get; set; }

        public AstarContext()
        {
        }

        public AstarContext(DbContextOptions<AstarContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder
                .AddCandidateEntityMapping()
                .AddLocationEntityMapping()
                .AddSkillEntityMapping()
                .AddCandidateSkillLinkEntityMapping();
        }
    }
}
