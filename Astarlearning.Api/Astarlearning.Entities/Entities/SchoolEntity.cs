using System.Collections.Generic;

namespace Astarlearning.Entities.Entities
{
    public class SchoolEntity : BaseEntity
    {
        public virtual ICollection<UserEntity> Users { get; set; }

        public virtual ICollection<ModuleEntity> Modules { get; set; }
    }
}