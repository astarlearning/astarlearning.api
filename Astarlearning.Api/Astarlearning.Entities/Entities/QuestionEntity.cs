using System.Collections.Generic;

namespace Astarlearning.Entities.Entities
{
    public class QuestionEntity : BaseEntity
    {
        public string Question { get; set; }

        public string Answer { get; set; }

        public ICollection<QuestionTestEntity> Tests = new List<QuestionTestEntity>();
    }
}