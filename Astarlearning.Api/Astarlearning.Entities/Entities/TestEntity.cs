using System.Collections.Generic;

namespace Astarlearning.Entities.Entities
{
    public class TestEntity : BaseEntity
    {
        public virtual ModuleEntity Module { get; set; }

        public virtual ICollection<QuestionTestEntity> Questions { get; set; } = new List<QuestionTestEntity>();
    }
}