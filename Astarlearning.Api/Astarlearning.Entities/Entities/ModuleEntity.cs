using System.Collections.Generic;

namespace Astarlearning.Entities.Entities
{
    public class ModuleEntity : BaseEntity
    {
        public virtual SchoolEntity School { get; set; }

        public virtual ICollection<TestEntity> Tests { get; set; } = new List<TestEntity>();
    }
}