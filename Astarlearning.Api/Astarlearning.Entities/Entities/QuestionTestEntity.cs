namespace Astarlearning.Entities.Entities
{
    public class QuestionTestEntity : BaseEntity
    {
        public QuestionEntity Question { get; set; }

        public TestEntity Test { get; set; }
    }
}