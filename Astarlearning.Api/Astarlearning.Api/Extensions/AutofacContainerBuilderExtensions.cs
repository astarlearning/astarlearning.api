﻿using System;
using System.Reflection;
using Autofac;
using Autofac.Builder;
using Autofac.Features.Scanning;

namespace Astarlearning.Api.Extensions
{
    public static class AutofacContainerBuilderExtensions
    {
        public static ContainerBuilder ConfigureDependencies(this ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(AppDomain.CurrentDomain.GetAssemblies())
                .WithAttribute<DomainServiceAttribute>()
                .AsImplementedInterfaces()
                .InstancePerDependency();

            return builder;
        }

        public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> WithAttribute<TAttribute>(
            this IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> builder) where TAttribute : Attribute
        {
            return builder.Where(e => e.GetCustomAttribute<TAttribute>() != null);
        }
    }
}
