﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Astarlearning.Api.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDbContextConfiguration(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            var connection = configuration.GetConnectionString("DefaultConnection");
            var test = configuration.ToString();
            services.AddDbContext<PickrTestContext>(options => options.UseSqlite(connection));

            return services;
        }

        public static IServiceCollection AddAppSettings(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<JobsConfig>(configuration.GetSection("JobsConfig"));

            return services;
        }
    }
}
