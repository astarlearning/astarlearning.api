using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Astarlearning.Api.Controllers
{
    public class ModuleController
    {
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }
    }
}