﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Astarlearning.Domain.Domain.Answers.Models
{
    public class AnswerEditor
    {
        public string Answer { get; set; }

        public bool IsCorrect { get; set; }
    }
}
