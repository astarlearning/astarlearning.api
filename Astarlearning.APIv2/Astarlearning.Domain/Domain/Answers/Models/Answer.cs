﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Astarlearning.Domain.Domain.Answers.Models
{
    public class Answer
    {
        public string Value { get; set; }

        public bool IsCorrect { get; set; }
    }
}
