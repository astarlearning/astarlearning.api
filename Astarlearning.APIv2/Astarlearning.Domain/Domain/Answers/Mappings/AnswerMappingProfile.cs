﻿using Astarlearning.Domain.Data.Answers.Entities;
using Astarlearning.Domain.Data.Questions.Entities;
using Astarlearning.Domain.Domain.Answers.Models;
using Astarlearning.Domain.Domain.Questions.Models;
using AutoMapper;

namespace Astarlearning.Domain.Domain.Tests.Mappings
{
    public class AnswerMappingProfile : Profile
    {
        public AnswerMappingProfile()
        {
            CreateMap<AnswerEntity, Answer>()
                .ForMember(t => t.Value, a => a.MapFrom(s => s.Answer))
                .ForMember(t => t.IsCorrect, a => a.MapFrom(s => s.IsCorrect != 0));
        }
    }
}
