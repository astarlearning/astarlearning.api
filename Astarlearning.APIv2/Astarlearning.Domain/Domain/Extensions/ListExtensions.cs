﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Astarlearning.Domain.Domain.Extensions
{
    public static class ListExtensions
    {
        private static IList<T> Swap<T>(this IList<T> list, int indexA, int indexB)
        {
            T tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
            return list;
        }
    }
}
