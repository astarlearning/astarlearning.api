﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Astarlearning.Domain.Domain.Schools.Models
{
    public class SchoolEditor
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
