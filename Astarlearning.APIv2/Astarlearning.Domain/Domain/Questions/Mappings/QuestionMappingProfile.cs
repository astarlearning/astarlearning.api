﻿using Astarlearning.Domain.Data.Questions.Entities;
using Astarlearning.Domain.Data.Tests.Entities;
using Astarlearning.Domain.Domain.Questions.Models;
using Astarlearning.Domain.Domain.Tests.Models;
using AutoMapper;

namespace Astarlearning.Domain.Domain.Tests.Mappings
{
    public class QuestionMappingProfile : Profile
    {
        public QuestionMappingProfile()
        {
            CreateMap<QuestionEntity, Question>()
                .ForMember(t => t.Sum, a => a.MapFrom(s => s.Question));
        }
    }
}
