﻿using System;
using System.Collections.Generic;
using System.Text;
using Astarlearning.Domain.Domain.Answers.Models;

namespace Astarlearning.Domain.Domain.Questions.Models
{
    public class QuestionEditor
    {
        public string Sum { get; set; }

        public ICollection<AnswerEditor> Answers = new List<AnswerEditor>();
    }
}
