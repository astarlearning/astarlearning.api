﻿using System;
using System.Collections.Generic;
using System.Text;
using Astarlearning.Domain.Domain.Answers.Models;

namespace Astarlearning.Domain.Domain.Questions.Models
{
    public class Question
    {
        public string Sum { get; set; }

        public ICollection<Answer> Answers { get; set; } = new List<Answer>();
    }
}
