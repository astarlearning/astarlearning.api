﻿using System;
using System.Collections.Generic;
using System.Text;
using Astarlearning.Domain.Domain.Questions.Models;

namespace Astarlearning.Domain.Domain.Tests.Models
{
    public class Test
    {
        public string Name { get; set; }

        public ICollection<Question> Questions { get; set; } = new List<Question>();
    }
}
