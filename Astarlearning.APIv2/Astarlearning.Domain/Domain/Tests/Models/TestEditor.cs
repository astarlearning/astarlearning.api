﻿using System;
using System.Collections.Generic;
using System.Text;
using Astarlearning.Domain.Domain.Questions.Models;

namespace Astarlearning.Domain.Domain.Tests.Models
{
    public class TestEditor
    {
        public int ModuleId { get; set; }

        public string Name { get; set; }

        public ICollection<QuestionEditor> Questions = new List<QuestionEditor>();
    }
}
