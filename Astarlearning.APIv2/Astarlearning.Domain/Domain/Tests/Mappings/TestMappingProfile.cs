﻿using Astarlearning.Domain.Data.Tests.Entities;
using Astarlearning.Domain.Domain.Tests.Models;
using AutoMapper;

namespace Astarlearning.Domain.Domain.Tests.Mappings
{
    public class TestMappingProfile : Profile
    {
        public TestMappingProfile()
        {
            CreateMap<TestEntity, Test>()
                .ForMember(t => t.Name, a => a.MapFrom(s => "Test " + s.Id));
        }
    }
}
