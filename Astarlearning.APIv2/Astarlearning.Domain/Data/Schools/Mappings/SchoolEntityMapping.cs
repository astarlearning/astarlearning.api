﻿using System;
using System.Collections.Generic;
using System.Text;
using Astarlearning.Domain.Data.Schools.Entities;
using Microsoft.EntityFrameworkCore;

namespace Astarlearning.Domain.Data.Schools.Mappings
{
    public static class SchoolEntityMapping
    {
        public static ModelBuilder AddSchoolEntityMapping(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SchoolEntity>(entity =>
            {
                entity
                .HasMany(p => p.Users)
                .WithOne(p => p.School);
            });

            return modelBuilder;
        }
    }
}
