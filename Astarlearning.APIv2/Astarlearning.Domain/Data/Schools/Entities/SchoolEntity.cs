﻿using System;
using System.Collections.Generic;
using System.Text;
using Astarlearning.Domain.Data.Modules.Entities;
using Astarlearning.Domain.Data.Users.Entities;

namespace Astarlearning.Domain.Data.Schools.Entities
{
    public class SchoolEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<UserEntity> Users { get; set; } = new List<UserEntity>();

        public virtual ICollection<ModuleEntity> Modules { get; set; } = new List<ModuleEntity>();
    }
}
