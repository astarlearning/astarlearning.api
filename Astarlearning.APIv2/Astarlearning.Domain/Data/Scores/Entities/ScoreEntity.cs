﻿using Astarlearning.Domain.Data.Questions.Entities;
using Astarlearning.Domain.Data.Tests.Entities;

namespace Astarlearning.Domain.Data.Users.Entities
{
    public class ScoreEntity
    {
        public int Id { get; set; }

        public int Correct { get; set; }

        public UserEntity User { get; set; }

        public TestEntity Test { get; set; }
    }
}
