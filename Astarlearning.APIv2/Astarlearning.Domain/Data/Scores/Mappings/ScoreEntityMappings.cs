﻿using System;
using System.Collections.Generic;
using System.Text;
using Astarlearning.Domain.Data.Users.Entities;
using Microsoft.EntityFrameworkCore;

namespace Astarlearning.Domain.Data.Scores.Mappings
{
    public static class ScoreEntityMappings
    {
        public static ModelBuilder AddScoreEntityMapping(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ScoreEntity>(entity =>
            {
                entity
                    .HasOne(p => p.User)
                    .WithMany();

                entity
                    .HasOne(p => p.Test)
                    .WithMany();
            });

            return modelBuilder;
        }
    }
}
