﻿using Astarlearning.Domain.Data.Questions.Entities;

namespace Astarlearning.Domain.Data.Answers.Entities
{
    public class AnswerEntity
    {
        public int Id { get; set; }

        public string Answer { get; set; }

        public virtual QuestionEntity Question { get; set; }

        public int IsCorrect { get; set; }
    }
}
