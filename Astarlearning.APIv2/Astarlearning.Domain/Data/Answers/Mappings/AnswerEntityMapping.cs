﻿using System;
using System.Collections.Generic;
using System.Text;
using Astarlearning.Domain.Data.Answers.Entities;
using Astarlearning.Domain.Data.Modules.Entities;
using Astarlearning.Domain.Data.Questions.Entities;
using Astarlearning.Domain.Data.Schools.Entities;
using Microsoft.EntityFrameworkCore;

namespace Astarlearning.Domain.Data.Schools.Mappings
{
    public static class AnswerEntityMapping
    {
        public static ModelBuilder AddAnswerEntityMapping(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AnswerEntity>(entity =>
            {
                entity
                .HasOne(p => p.Question)
                .WithMany(p => p.Answers);
            });

            return modelBuilder;
        }
    }
}
