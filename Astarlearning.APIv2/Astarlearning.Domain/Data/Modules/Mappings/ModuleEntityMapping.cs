﻿using System;
using System.Collections.Generic;
using System.Text;
using Astarlearning.Domain.Data.Modules.Entities;
using Astarlearning.Domain.Data.Questions.Entities;
using Astarlearning.Domain.Data.Schools.Entities;
using Microsoft.EntityFrameworkCore;

namespace Astarlearning.Domain.Data.Schools.Mappings
{
    public static class ModuleEntityMapping
    {
        public static ModelBuilder AddModuleEntityMapping(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ModuleEntity>(entity =>
            {
                entity
                .HasMany(p => p.Tests)
                .WithOne(p => p.Module);
            });

            return modelBuilder;
        }
    }
}
