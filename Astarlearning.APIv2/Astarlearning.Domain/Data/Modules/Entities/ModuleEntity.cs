﻿using System;
using System.Collections.Generic;
using System.Text;
using Astarlearning.Domain.Data.Schools.Entities;
using Astarlearning.Domain.Data.Tests.Entities;

namespace Astarlearning.Domain.Data.Modules.Entities
{
    public class ModuleEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<TestEntity> Tests { get; set; } = new List<TestEntity>();
    }
}
