﻿using System;
using System.Collections.Generic;
using System.Text;
using Astarlearning.Domain.Data.Answers.Entities;
using Astarlearning.Domain.Data.Tests.Entities;

namespace Astarlearning.Domain.Data.Questions.Entities
{
    public class QuestionEntity
    {
        public int Id { get; set; }

        public string Question { get; set; }

        public virtual TestEntity Test { get; set; }

        public ICollection<AnswerEntity> Answers { get; set; } = new List<AnswerEntity>();
    }
}
