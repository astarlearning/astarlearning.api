﻿using System;
using System.Collections.Generic;
using System.Text;
using Astarlearning.Domain.Data.Tests.Entities;
using Microsoft.EntityFrameworkCore;

namespace Astarlearning.Domain.Data.Tests.Mappings
{
    public static class TestEntityMapping
    {
        public static ModelBuilder AddTestEntityMapping(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TestEntity>(entity =>
            {
                entity
                .HasMany(p => p.Questions)
                .WithOne(p => p.Test);
            });

            return modelBuilder;
        }
    }
}
