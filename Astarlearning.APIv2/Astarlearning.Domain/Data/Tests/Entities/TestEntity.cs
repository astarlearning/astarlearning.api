﻿using System.Collections.Generic;
using Astarlearning.Domain.Data.Modules.Entities;
using Astarlearning.Domain.Data.Questions.Entities;

namespace Astarlearning.Domain.Data.Tests.Entities
{
    public class TestEntity
    {
        public int Id { get; set; }

        public virtual ICollection<QuestionEntity> Questions { get; set; } = new List<QuestionEntity>();

        public virtual ModuleEntity Module { get; set; }
    }
}
