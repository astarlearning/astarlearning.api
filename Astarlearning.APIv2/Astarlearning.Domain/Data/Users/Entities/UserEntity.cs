﻿using System;
using System.Collections.Generic;
using System.Text;
using Astarlearning.Domain.Data.Schools.Entities;

namespace Astarlearning.Domain.Data.Users.Entities
{
    public class UserEntity
    {
        public int Id { get; set; }

        public virtual SchoolEntity School { get; set; }

        public bool LeaderboardEligible { get; set; }
    }
}
