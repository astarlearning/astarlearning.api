﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Astarlearning.Domain.Data;
using Astarlearning.Domain.Data.Modules.Entities;
using Astarlearning.Domain.Data.Schools.Entities;
using Astarlearning.Domain.Domain.Modules.Models;
using Astarlearning.Domain.Domain.Schools.Models;
using Astarlearning.Domain.Domain.Tests.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Astarlearning.APIv2.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ModuleController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly AstarlearningContext _context;

        public ModuleController(
            ILogger<WeatherForecastController> logger,
            AstarlearningContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public int Get()
        {
            return 1;
        }

        [HttpPost]
        public IActionResult Post(ModuleEditor editor)
        {
            var entity = new ModuleEntity()
            {
                Name = editor.Name
            };

            _context.Modules.Add(entity);
            _context.SaveChanges();
            return Ok();
        }

        public ICollection<ModuleEntity> getModules(int userId)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == userId);
            var school = user.School;
            var modules = school.Modules;
            return modules;
        }

        public ICollection<ModuleEntity> getAllModules()
        {
            var modules = _context.Modules.ToList();
            return modules;
        }
    }
}
