﻿using System;
using System.Collections.Generic;
using System.Linq;
using Astarlearning.Domain.Data.Answers.Entities;
using Astarlearning.Domain.Data.Questions.Entities;
using Astarlearning.Domain.Data.Tests.Entities;
using Astarlearning.Domain.Domain.Answers.Models;
using Astarlearning.Domain.Domain.Questions.Models;
using Astarlearning.Domain.Domain.Tests.Models;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;

namespace Astarlearning.APIv2.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestController : ControllerBase
    {
        private readonly AstarlearningContext _context;
        private readonly IMapper _mapper;

        public TestController(
            AstarlearningContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("{moduleId}")]
        [Route("")]
        public Test Get(int moduleId)
        {
            return new Test
            {
                Name = "Example Test",
                Questions = new List<Question>()
                {
                    new Question
                    {
                        Sum = "13 x 12",
                        Answers = new List<Answer>()
                        {
                           new Answer
                           {
                               Value = "156",
                               IsCorrect = true,
                           },
                           new Answer
                           {
                               Value = "147",
                               IsCorrect = false,
                           },
                           new Answer
                           {
                               Value = "140",
                               IsCorrect = false,
                           },
                           new Answer
                           {
                               Value = "150",
                               IsCorrect = false,
                           },
                           new Answer
                           {
                               Value = "160",
                               IsCorrect = false,
                           },
                           new Answer
                           {
                               Value = "143",
                               IsCorrect = false,
                           },
                           new Answer
                           {
                               Value = "123",
                               IsCorrect = false,
                           },
                           new Answer
                           {
                               Value = "169",
                               IsCorrect = false,
                           }
                        }
                    },
                    new Question
                    {
                        Sum = "12 x 11",
                        Answers = new List<Answer>()
                        {
                           new Answer
                           {
                               Value = "233",
                               IsCorrect = false,
                           },
                           new Answer
                           {
                               Value = "132",
                               IsCorrect = false,
                           },
                           new Answer
                           {
                               Value = "140",
                               IsCorrect = false,
                           },
                           new Answer
                           {
                               Value = "130",
                               IsCorrect = false,
                           },
                           new Answer
                           {
                               Value = "121",
                               IsCorrect = false,
                           },
                           new Answer
                           {
                               Value = "131",
                               IsCorrect = true,
                           },
                           new Answer
                           {
                               Value = "150",
                               IsCorrect = false,
                           },
                           new Answer
                           {
                               Value = "97",
                               IsCorrect = false,
                           }
                        }
                    }
                }
            };
        }

        [HttpGet]
        [Route("actual/{moduleId}")]
        public Test GetActual(int moduleId)
        {
            var tests = _mapper.ProjectTo<Test>(_context.Tests
                .Where(p => p.Module.Id == moduleId))
                .ToList();

            var random = new Random();
            var randomNumber = random.Next(0, tests.Count);

            var chosenTest = tests[randomNumber];

            var questionList = chosenTest.Questions.ToList();
            for (var i = 0; i < questionList.Count; i++)
            {
                questionList[i].Answers = questionList[i].Answers.OrderBy(x => random.Next()).ToList();
            }

            return chosenTest;
        }

        [HttpPost]
        public IActionResult Post(TestEditor editor)
        {
            var module = _context.Modules.FirstOrDefault(p => p.Id == editor.ModuleId);

            var entity = new TestEntity
            {
                Module = module,
            };

            foreach (var question in editor.Questions)
            {
                var questionEntity = new QuestionEntity
                {
                    Question = question.Sum
                };

                foreach (var answer in question.Answers)
                {
                    var answerEntity = new AnswerEntity
                    {
                        Answer = answer.Answer,
                        IsCorrect = answer.IsCorrect == true ? 1 : 0
                    };

                    questionEntity.Answers.Add(answerEntity);

                    _context.Answers.Add(answerEntity);
                }

                entity.Questions.Add(questionEntity);

                _context.Questions.Add(questionEntity);
            }

            _context.Tests.Add(entity);
            _context.SaveChanges();
            return Ok();
        }

        public static void Swap<T>(IList<T> list, int indexA, int indexB)
        {
            T tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
        }
    }
}
