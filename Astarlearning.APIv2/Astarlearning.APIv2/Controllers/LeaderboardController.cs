﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Astarlearning.Domain.Data;
using Astarlearning.Domain.Data.Modules.Entities;
using Astarlearning.Domain.Data.Schools.Entities;
using Astarlearning.Domain.Data.Users.Entities;
using Astarlearning.Domain.Domain.Modules.Models;
using Astarlearning.Domain.Domain.Schools.Models;
using Astarlearning.Domain.Domain.Tests.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Astarlearning.APIv2.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LeaderboardController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly AstarlearningContext _context;

        public LeaderboardController(
            ILogger<WeatherForecastController> logger,
            AstarlearningContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public int Get()
        {
            return 1;
        }


        [HttpGet]
        public IOrderedQueryable<ScoreEntity> GetLeaderboard(int testId)
        {
            var test = _context.Tests.FirstOrDefault(t => t.Id == testId);
            if(test == null)
            {
                return null;
            }
            var scores = _context.Scores.Where(t => t.Test == test).OrderBy(s => s.Correct);

            return scores;

        }

    }
}