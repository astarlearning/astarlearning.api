﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Astarlearning.Domain.Data;
using Astarlearning.Domain.Data.Schools.Entities;
using Astarlearning.Domain.Domain.Schools.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Astarlearning.APIv2.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly AstarlearningContext _context;

        public WeatherForecastController(
            ILogger<WeatherForecastController> logger,
            AstarlearningContext context
            )
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public int Get()
        {
            return _context.Schools.Count();
        }

        [HttpPost]
        public IActionResult Post(SchoolEditor editor)
        {
            var entity = new SchoolEntity()
            {
                Name = editor.Name
            };

            _context.Schools.Add(entity);
            _context.SaveChanges();
            return Ok();
        }
    }
}
