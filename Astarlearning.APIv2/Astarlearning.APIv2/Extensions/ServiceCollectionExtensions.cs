﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Astarlearning.APIv2.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDbContextConfiguration(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            var connection = configuration.GetConnectionString("DefaultConnection");
            var test = configuration.ToString();
            services.AddDbContext<AstarlearningContext>(options => options.UseSqlite(connection));

            return services;
        }
    }
}
