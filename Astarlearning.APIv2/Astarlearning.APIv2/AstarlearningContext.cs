﻿using System.IO;
using Astarlearning.Domain.Data.Answers.Entities;
using Astarlearning.Domain.Data.Modules.Entities;
using Astarlearning.Domain.Data.Questions.Entities;
using Astarlearning.Domain.Data.Schools.Entities;
using Astarlearning.Domain.Data.Schools.Mappings;
using Astarlearning.Domain.Data.Tests.Entities;
using Astarlearning.Domain.Data.Tests.Mappings;
using Astarlearning.Domain.Data.Users.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Astarlearning.APIv2
{
    public class AstarlearningContext : DbContext
    {
        public virtual DbSet<SchoolEntity> Schools { get; set; }

        public virtual DbSet<UserEntity> Users { get; set; }

        public virtual DbSet<QuestionEntity> Questions { get; set; }

        public virtual DbSet<TestEntity> Tests { get; set; }

        public virtual DbSet<ModuleEntity> Modules { get; set; }

        public virtual DbSet<AnswerEntity> Answers { get; set; }

        public virtual DbSet<ScoreEntity> Scores { get; set; }

        public AstarlearningContext()
        {
        }

        public AstarlearningContext(DbContextOptions<AstarlearningContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                    .AddSchoolEntityMapping()
                    .AddModuleEntityMapping()
                    .AddTestEntityMapping()
                    .AddAnswerEntityMapping();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json")
                   .Build();

                var connectionString = configuration.GetConnectionString("DefaultConnection");
                optionsBuilder.UseSqlite(connectionString);
            }
        }
    }
}
